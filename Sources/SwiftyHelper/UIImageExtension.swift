//
//  UIImageExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy on 01/10/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import UIKit

public extension UIImage {
    
    enum CompareImage {
        case similar
        case notSimilar
        case notFound
    }
    
    /// Compares two images with PNG
    ///
    /// - Parameter image: second image to compare
    /// - Returns: CompareImage value
    func isEqualToImage(image: UIImage) -> CompareImage {
        guard let data1 = self.pngData() as NSData? else {
            return .notFound
        }
        guard let data2 = image.pngData() as NSData? else {
            return .notFound
        }
        return data1.isEqual(data2) ? .similar : .notSimilar
    }
    
}

public extension UIImage {
    
    
    /// Create an image with just color
    ///
    /// - Parameters:
    ///   - color: UIColor for image
    ///   - size: size of the image, default to 1, 1
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

public extension Optional where Wrapped == UIImage {
    
    func toBase64(to expectedSizeKB: Int = 1024) -> String? {
        guard let image = self else {return nil}
        let compareImage = image.isEqualToImage(image: #imageLiteral(resourceName: "camera"))
        if compareImage == .notFound || compareImage == .similar {
            return nil
        }
        let imageData = image.compressImage(to: expectedSizeKB)
        return imageData?.base64EncodedString()
    }
}

public extension UIImage {
    
    func compressImage(to expectedSizeKB: Int) -> Data? {
        let sizeInBytes = expectedSizeKB * 1024
        var needCompress: Bool = true
        var imageData: Data?
        var compressingValue: CGFloat = 1.0
        while(needCompress && compressingValue >= 0.0) {
            if let data: Data = self.jpegData(compressionQuality: compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imageData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if imageData == nil {
            imageData = self.jpegData(compressionQuality: compressingValue)
        }
        
        return imageData
    }
}
