//
//  StringExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public extension String {

    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func toDate(_ format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}

public extension String {

    func fileName() -> String {

        if let fileNameWithoutExtension = NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent {
            return fileNameWithoutExtension
        } else {
            return ""
        }
    }

    func fileExtension() -> String {

        if let fileExtension = NSURL(fileURLWithPath: self).pathExtension {
            return fileExtension
        } else {
            return ""
        }
    }
}

public extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Medium", size: 15)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }

    @discardableResult func normal(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: 15)!]
        let normal =  NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normal)
        return self
    }

    @discardableResult func largeBold(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Medium", size: 18)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }

    @discardableResult func largeNormal(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: 18)!]
        let normal =  NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normal)
        return self
    }
}

public extension String {

    
    /// Calculates height of given text based on font size and width of text area
    ///
    /// - Parameters:
    ///   - fontSize: default to 15.0
    ///   - width: CGFloat value of text area
    /// - Returns: CGFloat height value
    func calculateHeight(fontSize: CGFloat = 15.0, width: CGFloat) -> CGFloat {
        let messageString = self
        let attributes:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: fontSize)!]

        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)

        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)

        let requredSize:CGRect = rect
        return requredSize.height
    }

    /// Calculates width of given text based on font size
    ///
    /// - Parameters:
    ///   - fontSize: default to 15.0
    /// - Returns: CGFloat width value
    func calculateWidth(fontSize: CGFloat = 15.0) -> CGFloat {
        let messageString = self
        let attributes:[NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.font : UIFont(name: "AvenirNext-Regular", size: fontSize)!]

        let attributedString : NSAttributedString = NSAttributedString(string: messageString, attributes: attributes)

        return attributedString.size().width
    }
}
