//
//  DateExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation

public extension Foundation.Calendar {

    static var newCurrent: Foundation.Calendar {
        var calendar = Foundation.Calendar.current
        calendar.timeZone = TimeZone.current
        calendar.firstWeekday = 2
        return calendar
    }
}

public extension Date {

    static func monthAdvanced(number: Int) -> Date {
        return Foundation.Calendar.newCurrent.date(byAdding: .month, value: number, to: Date())!
    }

    static func daysAdvanced(number: Int) -> Date {
        return Foundation.Calendar.newCurrent.date(byAdding: .day, value: number, to: Date())!
    }

    static func weeksAdvanced(number: Int) -> Date {
        return Foundation.Calendar.newCurrent.date(byAdding: .weekday, value: number, to: Date())!
    }
}

public extension Date {

    func monthAdvanced(number: Int) -> Date {
        return Foundation.Calendar.newCurrent.date(byAdding: .month, value: number, to: self) ?? self
    }

    func daysAdvanced(number: Int) -> Date {
        return Foundation.Calendar.newCurrent.date(byAdding: .day, value: number, to: self) ?? self
    }

    func weeksAdvanced(number: Int) -> Date {
        return Foundation.Calendar.current.date(byAdding: .weekday, value: number, to: self) ?? self
    }

    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }

    var startOfWeek: Date {
        guard let monday = Foundation.Calendar.newCurrent.date(from: Foundation.Calendar.newCurrent.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return self }
        return monday
    }

    var startOfDay: Date {
        return Foundation.Calendar.newCurrent.startOfDay(for: self)
    }

    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Foundation.Calendar.newCurrent.date(byAdding: components, to: startOfDay)!
    }

    var startOfMonth: Date {
        let components = Foundation.Calendar.newCurrent.dateComponents([.year, .month], from: startOfDay)
        return Calendar.current.date(from: components)!
    }
    var endOfMonth: Date {
        var components = DateComponents()
        components.month = 1
        components.second = -1
        return Foundation.Calendar.newCurrent.date(byAdding: components, to: startOfMonth)!
    }
}

public extension Date {

    func startOf(_ dateComponent : Calendar.Component) -> Date {
        var startOfComponent = self
        var timeInterval : TimeInterval = 0.0
        let _ = Foundation.Calendar.newCurrent.dateInterval(of: dateComponent, start: &startOfComponent, interval: &timeInterval, for: self)
        return startOfComponent
    }

    func getMonthShortName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
}

public extension Date {
    var weekdayIndex: Int {
        let weekDay = Calendar.current.component(.weekday, from: self)
        //Sunday = 1
        switch weekDay {
        case 7:
            return 5
        case 1:
            return 6
        case 2:
            return 0
        case 3:
            return 1
        case 4:
            return 2
        case 5:
            return 3
        case 6:
            return 4
        default:
            return 0
        }
    }

    var weekday: Int {
        return Foundation.Calendar.newCurrent.component(.weekday, from: self)
    }

    var day: Int {
        return Foundation.Calendar.newCurrent.component(.day, from: self)
    }

    var minute: Int {
        return Foundation.Calendar.newCurrent.component(.minute, from: self)
    }

    var hour: Int {
        return Foundation.Calendar.newCurrent.component(.hour, from: self)
    }

    var second: Int {
        return Foundation.Calendar.newCurrent.component(.second, from: self)
    }

    var month: Int {
        return Foundation.Calendar.newCurrent.component(.month, from: self)
    }
}

public extension Date {

    func compareDateMonthAndDay(from date: Date ) -> Bool {

        let fromDay = date.day
        let fromMonth = date.month

        let nowDay = self.day
        let nowMonth = self.month

        if (fromDay != nowDay) || (fromMonth != nowMonth) {
            return true
        }

        return false
    }
}

public extension Date {

    func isDateInTodate() -> Bool {
        return Foundation.Calendar.newCurrent.isDateInToday(self)
    }

    func isDateInYesterday() -> Bool {
        return Foundation.Calendar.newCurrent.isDateInYesterday(self)
    }

    func isDateInTomorrow() -> Bool {
        return Foundation.Calendar.newCurrent.isDateInTomorrow(self)
    }
    func isDateInWeekend() -> Bool {
        return Foundation.Calendar.newCurrent.isDateInWeekend(self)
    }

    func isDateEqualTo(_ date: Date, by component: Calendar.Component) -> Bool {
        return Foundation.Calendar.newCurrent.isDate(self, equalTo: date, toGranularity: component)
    }

}
