//
//  UIViewControllerExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//
import UIKit

public extension UIViewController {

    private struct ActivityView {
        static let spinner = UIActivityIndicatorView(style: .gray)
    }

    /// Added keyboard dimiss gesture to view
    func addKeyboardDismissGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }
    
    var isSpinnerAnimating: Bool {
        return ActivityView.spinner.isAnimating
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }

    
    /// Remove all view gestures
    func removeAllGestures() {
        for recognizer in self.view.gestureRecognizers ?? [] {
            self.view.removeGestureRecognizer(recognizer)
        }
    }

    
    /// Show and start activity spinner
    ///
    /// - Parameter isWhite: If you want the activity spinner to be white
    func showAndStartSpinner(_ isWhite: Bool = false) {
        ActivityView.spinner.center = self.view.center
        ActivityView.spinner.scale(factor: 3)
        ActivityView.spinner.startAnimating()
        ActivityView.spinner.style = isWhite ? .whiteLarge : .gray
        ActivityView.spinner.hidesWhenStopped = true
        self.view.addSubview(ActivityView.spinner)
    }

    /// Stop and remove spinner from view
    func stopSpinnerAndRemove() {
        ActivityView.spinner.stopAnimating()
    }
}
