//
//  UIViewController+TextFieldDelegate.swift
//  SwiftyHelpers
//
//  Created by Timothy on 29/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import UIKit

extension UIViewController: UITextFieldDelegate {
    
    /// Keyboard search struct with closure called searchChange
    public struct KeyboardSearch {
        /// searchChange closure with text value
        public static var searchChange: ((_ text: String) -> Void)?
    }
    
    
    /// Show the keyboard with search bar on top and done button on the right/
    public func showKeyboardSeachBar() {
        let tempTextField = UITextField(frame: CGRect.zero)
        tempTextField.isHidden = true
        tempTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.view.addSubview(tempTextField)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let searchTextField = UITextField(frame: CGRect(x: 0, y: 0, width: self.view.frame.width * 0.7, height: toolBar.frame.height + 20))
        let searchButton = UIBarButtonItem(customView: searchTextField)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donePressed))
        toolBar.setItems([searchButton, spaceButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        tempTextField.delegate = self
        tempTextField.inputAccessoryView = toolBar
        
        tempTextField.becomeFirstResponder()
    }
    
    @objc private func donePressed() {
        view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else {return}
        KeyboardSearch.searchChange?(text)
    }
}
