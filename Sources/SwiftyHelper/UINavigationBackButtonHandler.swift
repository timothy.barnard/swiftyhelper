//
//  UINavigationBackButtonHandler.swift
//  DIT Timetable v3
//
//  Created by Timothy on 05/09/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public protocol  UINavigationBarBackButtonHandler {
    func  shouldPopOnBackButton() -> Bool
}

extension UIViewController: UINavigationBarBackButtonHandler {
    @objc public func  shouldPopOnBackButton() -> Bool {
        return true
    }
}

public extension UINavigationController {

    func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
        guard let items = navigationBar.items else {
            return false
        }

        if viewControllers.count < items.count {
            return true
        }

        var shouldPop = true
        if let vc = topViewController, vc.responds(to: #selector(UIViewController.shouldPopOnBackButton)) {
            shouldPop = vc.shouldPopOnBackButton()
        }

        if shouldPop {
            DispatchQueue.main.async {
                self.popViewController(animated: true)
            }
        } else {
            for aView in navigationBar.subviews {
                if aView.alpha > 0 && aView.alpha < 1 {
                    aView.alpha = 1.0
                }
            }
        }
        return false
    }
}
