//
//  ArrayExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy on 09/09/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation

public extension Array {
    
    /// Checks whether index is part of the array
    ///
    /// - Parameter index: Index
    subscript (safe index: Index) -> Element? {
        return 0 <= index && index < count ? self[index] : nil
    }
    
    /// Removes the first element matching the handler completion on the array itself
    ///
    /// - Parameter handler: handler
    /// - Returns: return element if found and removed
    @discardableResult
    mutating func remove(_ handler: (Element) -> Bool) -> Element? {
        guard let idx = firstIndex(where: handler) else {return nil}
        let item = self[idx]
        remove(at: idx)
        return item
    }
    
    /// It removes the first element that matches the handler condition
    /// and returns new array
    ///
    /// - Parameter handler: handler
    /// - Returns: new array of elements left
    func removed(_ handler: (Element) -> Bool) -> Array {
        var items = self
        items.remove(handler)
        return items
    }
}
