//
//  Bundle.swift
//  SwiftyHelpers
//
//  Created by Timothy on 24/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import Foundation

public enum FileType {
    case png
    case jpeg
    case json
    case txt
}

public extension FileType {
    
    var name: String {
        switch self {
        case .png: return "png"
        case .jpeg: return "jpeg"
        case .json: return "json"
        case .txt: return "txt"
        }
    }
}

public extension Bundle {
    
    func readFile(name: String, type: FileType) -> String? {
        if let path = Bundle.main.path(forResource: name, ofType: type.name) {
            do {
                let contents = try String(contentsOfFile: path)
                return contents
            } catch {
                return nil
            }
        }
        return nil
    }
}
