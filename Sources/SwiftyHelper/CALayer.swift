//
//  CALayer.swift
//  DIT Timetable v3
//
//  Created by Timothy on 27/09/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import UIKit

public extension CALayer {
    
    
    /// Adding shadow with option of rounded courners if
    /// cornerRadius is greater than zero
    func addShadow() {
        self.shadowOffset = .zero
        self.shadowOpacity = 1
        self.shadowRadius = 2
        self.shadowColor = UIColor.black.cgColor
        self.masksToBounds = false
        if cornerRadius != 0 {
            addShadowWithRoundedCorners()
        }
    }
    
    /// Adds rounded coners and shadow if shadowOpacity is greater than zero
    ///
    /// - Parameter radius: CGFloat
    func roundCorners(radius: CGFloat) {
        self.cornerRadius = radius
        if shadowOpacity != 0 {
            addShadowWithRoundedCorners()
        }
    }
    
    private func addShadowWithRoundedCorners() {
        if let contents = self.contents {
            masksToBounds = false
            sublayers?.filter { $0.frame.equalTo( self.bounds ) }
                .forEach { $0.roundCorners(radius: self.cornerRadius) }
            self.contents = nil
            if let sublayer = sublayers?.first {
                //sublayer.name == Constants.contentLayerName {
                sublayer.removeFromSuperlayer()
            }
            let contentLayer = CALayer()
            //contentLayer.name = Constants.contentLayerName
            contentLayer.contents = contents
            contentLayer.frame = bounds
            contentLayer.cornerRadius = cornerRadius
            contentLayer.masksToBounds = true
            insertSublayer(contentLayer, at: 0)
        }
    }
}
