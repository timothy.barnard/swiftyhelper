//
//  UITableViewExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public extension UITableView {

    func register<T: UITableViewCell>(_: T.Type) {
        let nib = UINib(nibName: String(describing: T.self), bundle: nil)
        register(nib, forCellReuseIdentifier: String(describing: T.self))
    }

    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: T.self), for: indexPath as IndexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(String(describing: T.self))")
        }

        return cell
    }
}
